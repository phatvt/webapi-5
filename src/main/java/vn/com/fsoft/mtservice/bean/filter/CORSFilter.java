/**
 * 
 */
package vn.com.fsoft.mtservice.bean.filter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

/**
 * @author hungxoan
 *
 */
public class CORSFilter implements Filter {

	public static final String ACCESS_CONTROL_ALLOW_ORIGIN_NAME = "Access-Control-Allow-Origin";
	public static final String DEFAULT_ACCESS_CONTROL_ALLOW_ORIGIN_VALUE = "*";

	public static final String ACCESS_CONTROL_ALLOW_METHDOS_NAME = "Access-Control-Allow-Methods";
	public static final String DEFAULT_ACCESS_CONTROL_ALLOW_METHDOS_VALUE = "POST, GET, OPTIONS, DELETE";

	public static final String ACCESS_CONTROL_MAX_AGE_NAME = "Access-Control-Max-Age";
	public static final String DEFAULT_ACCESS_CONTROL_MAX_AGE_VALUE = "3600";

	public static final String ACCESS_CONTROL_ALLOW_HEADERS_NAME = "Access-Control-Allow-Headers";
	public static final String DEFAULT_ACCESS_CONTROL_ALLOW_HEADERS_VALUE = "Access-Control-Allow-Headers, Access-Control-Expose-Headers, Origin,Accept, "
			+ "X-Requested-With, Content-Type, Access-Control-Request-Method, "
			+ "Access-Control-Request-Headers, language, userid, token, sessionid ,"
			+ "clientSecret, Access-Control-Allow-Origin, X-Forwarded-For";

	private String accessControlAllowOrigin = DEFAULT_ACCESS_CONTROL_ALLOW_ORIGIN_VALUE;
	private String accessControlAllowMethods = DEFAULT_ACCESS_CONTROL_ALLOW_METHDOS_VALUE;
	private String accessControlAllowMaxAge = DEFAULT_ACCESS_CONTROL_MAX_AGE_VALUE;
	private String accessControlAllowHeaders = DEFAULT_ACCESS_CONTROL_ALLOW_HEADERS_VALUE;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest,
	 * javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {

		HttpServletResponse res = (HttpServletResponse) servletResponse;

		res.setHeader(ACCESS_CONTROL_ALLOW_ORIGIN_NAME, accessControlAllowOrigin);
		res.setHeader(ACCESS_CONTROL_ALLOW_METHDOS_NAME, accessControlAllowMethods);
		res.setHeader(ACCESS_CONTROL_MAX_AGE_NAME, accessControlAllowMaxAge);
		res.setHeader(ACCESS_CONTROL_ALLOW_HEADERS_NAME, accessControlAllowHeaders);

		filterChain.doFilter(servletRequest, servletResponse);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		String initParameterValue;
		Map<String, String> stringStringMap = initConfig();

		for (Map.Entry<String, String> stringStringEntry : stringStringMap.entrySet()) {
			initParameterValue = filterConfig.getInitParameter(stringStringEntry.getKey());

			// if the init paramiter value isn't null then set the value in the
			// correct http header
			if (initParameterValue != null) {
				try {
					getClass().getDeclaredField(stringStringEntry.getValue()).set(this, initParameterValue);
				} catch (IllegalAccessException | NoSuchFieldException ignored) {
				}
			}
		}
	}

	private Map<String, String> initConfig() {
		Map<String, String> result = new HashMap<>();

		result.put(ACCESS_CONTROL_ALLOW_ORIGIN_NAME, "accessControlAllowOrigin");
		result.put(ACCESS_CONTROL_ALLOW_METHDOS_NAME, "accessControlAllowMethods");
		result.put(ACCESS_CONTROL_MAX_AGE_NAME, "accessControlAllowMaxAge");
		result.put(ACCESS_CONTROL_ALLOW_HEADERS_NAME, "accessControlAllowHeaders");

		return result;
	}

}
