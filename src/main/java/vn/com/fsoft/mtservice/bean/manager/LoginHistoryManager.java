package vn.com.fsoft.mtservice.bean.manager;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import vn.com.fsoft.mtservice.constants.HttpConstant;
import vn.com.fsoft.mtservice.object.base.RepoStatus;
import vn.com.fsoft.mtservice.object.entities.LoginHistoryEntity;
import vn.com.fsoft.mtservice.repository.LoginHistoryRepo;


@Component("loginHistoryManager")
@DependsOn(value = { "loginHistoryRepo" })
public class LoginHistoryManager {

	@Autowired
	private LoginHistoryRepo repository;

	private List<LoginHistoryEntity> histories;
	private Integer databaseCount = -1;

	public LoginHistoryManager() {
		this.histories = new ArrayList<>();
	}

	public synchronized void addHistory(LoginHistoryEntity entity, HttpServletRequest request) {
		String ip = request.getHeader("X-Forwarded-For");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		entity.setIpAddress(ip);
		this.histories.add(entity);
	}

	public void pushLoginHistoryToDatabase() {
		synchronized (histories) {
			RepoStatus status = repository.addAll(histories);
			if (HttpConstant.CODE_SUCCESS.equals(status.getCode())) {
				this.histories.clear();
				this.initCount();
			}
		}
	}

	public Integer getCount() {
		if (databaseCount < 0) {
			this.initCount();
		}
		return databaseCount + histories.size();
	}

	public void initCount() {
		RepoStatus<Integer> repoStatus = repository.getAllCount();
		if (repoStatus.getObject() != null) {
			this.databaseCount = repoStatus.getObject();
		} else {
			this.databaseCount = 0;
		}
	}
}
