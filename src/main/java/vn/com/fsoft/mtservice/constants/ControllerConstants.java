package vn.com.fsoft.mtservice.constants;

/**
 * hungxoan
 */
public class ControllerConstants {

    public static final String QUERY = "query";
    public static final String COMMAND = "command";

    public static class CommandType {

        public static final String CREATE = "n";
        public static final String UPDATE = "u";
        public static final String MERGE = "m";
        public static final String DELETE = "d";
    }
}
