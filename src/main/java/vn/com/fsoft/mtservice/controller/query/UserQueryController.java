package vn.com.fsoft.mtservice.controller.query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Component;

import vn.com.fsoft.mtservice.bean.data.UserRepo;
import vn.com.fsoft.mtservice.constants.HttpConstant;
import vn.com.fsoft.mtservice.object.base.IncomingRequestContext;
import vn.com.fsoft.mtservice.object.base.QueryResponseStatus;
import vn.com.fsoft.mtservice.object.base.RepoStatus;
import vn.com.fsoft.mtservice.object.base.Status;
import vn.com.fsoft.mtservice.object.entities.UserEntity;
import vn.com.fsoft.mtservice.object.form.UserForm;
import vn.com.fsoft.mtservice.object.helper.QueryResult;
import vn.com.fsoft.mtservice.util.JacksonUtils;
import vn.com.fsoft.mtservice.util.Views;

import java.util.List;

/**
 * 
 * @author hungxoan
 *
 */

@Component("userQueryController")
public class UserQueryController extends QueryControllerTemplate<UserForm>  {

    @Autowired
    private UserRepo userRepo;

    @Override
    protected String getDefinition(IncomingRequestContext context, RequestEntity<UserForm> requestEntity) {
        return null;
    }

    @Override
    protected String getDefinition(IncomingRequestContext context, Integer id, RequestEntity<UserForm> requestEntity) {
        return null;
    }

    @Override
    protected String findOneDefinition(IncomingRequestContext context, String key, RequestEntity<UserForm> requestEntity) {
        return null;
    }

    @SuppressWarnings("unchecked")
	@Override
    protected String queryDefinition(IncomingRequestContext context, RequestEntity<UserForm> requestEntity) {

        List<UserEntity> entityList = null;
        Integer rowCount = 0;
        UserForm form = requestEntity.getBody();

        RepoStatus<QueryResult<UserEntity>> repoStatus = userRepo.query(form);

        if(repoStatus == null) {
            return JacksonUtils.java2Json(new Status("500",
                    "Server got error. Please wait a moment and try it again later."));
        }

        if(repoStatus.getObject() != null &&
                repoStatus.getCode().equals(HttpConstant.CODE_SUCCESS)) {

            return JacksonUtils.java2Json(new QueryResponseStatus("200",
                            "found", repoStatus.getObject().getRowCount(),
                            repoStatus.getObject().getEntityList()),
                    Views.Public.class);
        }

        return JacksonUtils.java2Json(new Status(repoStatus.getCode(),
                repoStatus.getMessage()));
    }
}
