package vn.com.fsoft.mtservice.object.base;

/**
 * 
 * @author hungxoan
 *
 * @param <T>
 */
public class RepoStatus<T extends Object> extends Status {

    private T object;

    public RepoStatus() {
    }

    public RepoStatus(String code, String message) {
        super(code, message);
    }

    public RepoStatus(String code, String message, T object) {
        super(code, message);
        this.object = object;
    }

    public T getObject() {
        return object;
    }

    public void setObject(T object) {
        this.object = object;
    }
}
