package vn.com.fsoft.mtservice.object.constant.enumeration;

/**
 * hungxoan
 */
public enum DomainEnum {

    USER("users"),
    ROLE("roles"),
    PROJECT("projects"),
    ASSESSMENT_STANDARD("assessment_standard"),
    WST_COLLECTION("wst_collection"),
    REPORT_TEMPLATE("report_template");

    private String table;

    DomainEnum(String table) {
        this.table = table;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }
}
