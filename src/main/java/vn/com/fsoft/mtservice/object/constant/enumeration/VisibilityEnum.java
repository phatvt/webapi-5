package vn.com.fsoft.mtservice.object.constant.enumeration;

/**
 * hungxoan
 */
public enum VisibilityEnum {

    ACTIVE("active"),
    INACTIVE("inactive"),
    WORKING("working"),
    LIVE("live"),
    OPEN("open"),
    START("start"),
    LOCK("lock"),
    ANALYZING("analyze"),
    CLOSE("close"),
    CANCEL("cancel"),

    ;

    private String status;

    VisibilityEnum(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
