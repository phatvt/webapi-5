package vn.com.fsoft.mtservice.object.entities;


import javax.persistence.*;

import vn.com.fsoft.mtservice.constants.DatabaseConstants;

/**
 * 
 * @author hungxoan
 *
 */

@Entity(name = "CredentialTokenEntity")
@Table(name = "tokens", schema = DatabaseConstants.SCHEMA)
public class CredentialTokenEntity extends TrackingFieldEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "tokens_generator",
            strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "tokens_generator", schema = DatabaseConstants.SCHEMA,
            sequenceName = "tokens_seq")
    private Integer id;

    @Column(name = "userId")
    private Integer userId;

    @Column(name = "token")
    private String token;

    @Column(name = "nonce")
    private String nonce;

    public CredentialTokenEntity() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }
}
