package vn.com.fsoft.mtservice.object.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import vn.com.fsoft.mtservice.constants.DatabaseConstants;


@Entity(name = "LoginHistoryEntity")
@Table(name = "login_history", schema = DatabaseConstants.SCHEMA)
public class LoginHistoryEntity extends HibernateRootEntity {

	@Id
	@Column(name = "id")
	@GeneratedValue(generator = "login_history_value_generator", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "login_history_value_generator", schema = DatabaseConstants.SCHEMA, sequenceName = "login_history_seq")
	private Integer id;

	@Column(name = "user_name")
	private String userName;

	@Column(name = "success")
	private Boolean loginSuccess;

	@Column(name = "login_time")
	private Date loginTime;
	
	@Column(name = "ip_address")
	private String ipAddress;

	public LoginHistoryEntity() {
		super();
	}

	public LoginHistoryEntity(String userName, Boolean loginSuccess, Date loginTime) {
		super();
		this.userName = userName;
		this.loginSuccess = loginSuccess;
		this.loginTime = loginTime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Boolean getLoginSuccess() {
		return loginSuccess;
	}

	public void setLoginSuccess(Boolean loginSuccess) {
		this.loginSuccess = loginSuccess;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	
}
