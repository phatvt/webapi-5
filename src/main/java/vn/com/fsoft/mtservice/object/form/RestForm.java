package vn.com.fsoft.mtservice.object.form;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * @author hungxoan
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RestForm {

    private Integer userId; // userId
    private String t; // type
    private Integer p; // page
    private Integer pSize;

    public RestForm() {
        super();
    }

    public RestForm(Integer userId, String t, Integer p, Integer pSize) {

        super();
        this.userId = userId;
        this.t = t;
        this.p = p;
        this.pSize = pSize;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getT() {
        return t;
    }

    public void setT(String t) {
        this.t = t;
    }

    public Integer getP() {
        return p;
    }

    public void setP(Integer p) {
        this.p = p;
    }

    public Integer getpSize() {
        return pSize;
    }

    public void setpSize(Integer pSize) {
        this.pSize = pSize;
    }
}
