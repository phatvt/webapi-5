package vn.com.fsoft.mtservice.object.form;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import vn.com.fsoft.mtservice.object.form.command.UserCommandForm;
import vn.com.fsoft.mtservice.object.form.query.UserQueryForm;

/**
 * 
 * @author hungxoan
 *
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserForm extends RestForm  {

    private UserCommandForm model;
    private UserQueryForm query;
    private AuthenticationForm authentication;

    public UserForm() {
    }

    public UserCommandForm getModel() {
        return model;
    }

    public void setModel(UserCommandForm model) {
        this.model = model;
    }

    public UserQueryForm getQuery() {
        return query;
    }

    public void setQuery(UserQueryForm query) {
        this.query = query;
    }

    public AuthenticationForm getAuthentication() {
        return authentication;
    }

    public void setAuthentication(AuthenticationForm authentication) {
        this.authentication = authentication;
    }
}
