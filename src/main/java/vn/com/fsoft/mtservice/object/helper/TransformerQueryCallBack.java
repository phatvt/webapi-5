package vn.com.fsoft.mtservice.object.helper;

import org.hibernate.HibernateException;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;
import org.springframework.orm.hibernate5.HibernateCallback;

import vn.com.fsoft.mtservice.object.base.ResultTransformer;

import java.util.Collection;
import java.util.List;

/**
 * 
 * @author hungxoan
 *
 * @param <T>
 */
public class TransformerQueryCallBack<T extends ResultTransformer>
        implements HibernateCallback<List<T>> {

    private Integer rowCount;
    private String queryString;
    private String[] paramNames;
    private Object[] values;

    private int firstResult;
    private int maxResults;
    private Class clazz;
    private String scalarClassName;

    public TransformerQueryCallBack(String queryString, String[] paramNames, Object[] values,
                                         int firstResult, int maxResults, Class clazz,
                                    String scalarClassName) {

        this.queryString = queryString;
        this.paramNames = paramNames;
        this.values = values;
        this.firstResult = firstResult;
        this.maxResults = maxResults;
        this.clazz = clazz;
        this.scalarClassName = scalarClassName;
        this.rowCount = -1;
    }

    public TransformerQueryCallBack(String queryString, String[] paramNames, Object[] values,
                                    Class clazz, String scalarClassName) {

        this.queryString = queryString;
        this.paramNames = paramNames;
        this.values = values;
        this.clazz = clazz;
        this.scalarClassName = scalarClassName;
        this.rowCount = -1;
    }

    @Override
    public List<T> doInHibernate(Session session) throws HibernateException {

        NativeQuery query = session.createNativeQuery(queryString);

        for (int c=0; c< paramNames.length; c++) {
            applyNamedParameterToQuery(query, paramNames[c], values[c]);
        }

        ScrollableResults scroll = query.scroll();
        scroll.last();

        rowCount = scroll.getRowNumber() + 1;

        if(firstResult != 0) {
            query.setFirstResult(firstResult);
        }

        if(maxResults != 0) {
            query.setMaxResults(maxResults);
        }
        //HungPV comment this scalar
        //  ScalarStrategy strategy = ScalarStrategyCoordinator.getStrategy(scalarClassName);
       // query = strategy.setScalar(query);

        List<T> result = query.setResultTransformer(Transformers.aliasToBean(clazz)).getResultList();
        return result;
    }

    protected void applyNamedParameterToQuery(Query queryObject, String paramName, Object value)
            throws HibernateException {

        if (value instanceof Collection) {
            queryObject.setParameterList(paramName, (Collection) value);
        }
        else if (value instanceof Object[]) {
            queryObject.setParameterList(paramName, (Object[]) value);
        }
        else {
            queryObject.setParameter(paramName, value);
        }
    }

    public Integer getRowCount() {
        return rowCount;
    }

    public void setRowCount(Integer rowCount) {
        this.rowCount = rowCount;
    }
}
