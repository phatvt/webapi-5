package vn.com.fsoft.mtservice.object.models;

/**
 * 
 * @author hungxoan
 *
 */
public class PermissionModel {

    private Integer id;
    private String code;
    private String name;

    public PermissionModel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
